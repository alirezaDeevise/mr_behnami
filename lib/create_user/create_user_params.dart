class CreateUserParams {
  String? firstName;
  String? lastName;
  String? email;
  String? pushId;

  CreateUserParams({this.firstName, this.lastName, this.email, this.pushId});

  CreateUserParams.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    pushId = json['pushId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['pushId'] = this.pushId;
    return data;
  }
}
