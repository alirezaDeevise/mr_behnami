import 'package:flutter/cupertino.dart';

import '../create_user_result.dart';

abstract class States {}

class Empty extends States {}

class Loading extends States {}

class Loaded extends States {
  final CreateUserResult createUserResult;

  Loaded(this.createUserResult);
}

class Error extends States {
  final String error;
  Error(this.error);
}
