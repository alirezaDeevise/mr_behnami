import 'package:behnami/create_user/bloc/bloc_events.dart';
import 'package:behnami/create_user/bloc/bloc_states.dart';
import 'package:behnami/create_user/create_user_params.dart';
import 'package:behnami/create_user/create_user_result.dart';
import 'package:bloc/bloc.dart';

import '../../http_utils.dart';

class CreateUserBloc extends Bloc<Events, States> {
  CreateUserBloc() : super(Empty());

  @override
  Stream<States> mapEventToState(event) async* {
    if (event is CreateUserEvents) {
      yield Loading();
      HttpUtils httpUtils = HttpUtils();
      try {
        CreateUserResult requestResult =
            await getDataFromServer(httpUtils, event);
        yield Loaded(requestResult);
      } catch (e) {
        yield Error(e.toString());
      }
    }
  }

  Future<CreateUserResult> getDataFromServer(
      HttpUtils httpUtils, CreateUserEvents event) async {
    return await httpUtils.postData<CreateUserResult>(
        url: "http://invoice.api.deevise.com/users/createUser",
        parseData: (responseData) => CreateUserResult.fromJson(responseData),
        body: CreateUserParams(
                email: event.email,
                firstName: event.firstName,
                lastName: event.lastName,
                pushId: "-1")
            .toJson());
  }
}
