abstract class Events {}

class CreateUserEvents extends Events {
  final String? email;
  final String? lastName;
  final String? firstName;

  CreateUserEvents(this.email, this.lastName, this.firstName);
}
