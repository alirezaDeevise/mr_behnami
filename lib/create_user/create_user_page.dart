import 'package:behnami/create_user/bloc/bloc_bloc.dart';
import 'package:behnami/create_user/bloc/bloc_states.dart';
import 'package:behnami/create_user/create_user_params.dart';
import 'package:behnami/create_user/create_user_result.dart';
import 'package:behnami/http_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc_events.dart';

class CreateUserPage extends StatefulWidget {
  const CreateUserPage({Key? key}) : super(key: key);

  @override
  _CreateUserPageState createState() => _CreateUserPageState();
}

class _CreateUserPageState extends State<CreateUserPage> {
  String? firstName;
  String? lastName;
  String? email;

  CreateUserResult? createUserResult;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create user"),
      ),
      body: BlocProvider<CreateUserBloc>(
        create: (blocContext) => CreateUserBloc(),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                onChanged: (value) {
                  setState(() {
                    firstName = value;
                  });
                },
                decoration: InputDecoration(hintText: "First name"),
              ),
              SizedBox(
                height: 16.0,
              ),
              TextField(
                onChanged: (value) {
                  setState(() {
                    lastName = value;
                  });
                },
                decoration: InputDecoration(hintText: "Last name"),
              ),
              SizedBox(
                height: 16.0,
              ),
              TextField(
                onChanged: (value) {
                  setState(() {
                    email = value;
                  });
                },
                decoration: InputDecoration(hintText: "E-mail"),
              ),
              SizedBox(
                height: 32.0,
              ),
              BlocBuilder<CreateUserBloc, States>(
                builder: (context, state) {
                  return Column(
                    children: [
                      TextButton(
                          onPressed: () async {
                            BlocProvider.of<CreateUserBloc>(context).add(
                                CreateUserEvents(email, lastName, firstName));
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.blue)),
                          child: Center(
                            child: Text(
                              "Create user",
                              style: TextStyle(color: Colors.white),
                            ),
                          )),
                      SizedBox(
                        height: 16.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Id :"),
                          state is Loading
                              ? Text("Loading...")
                              : state is Loaded
                                  ? Text(
                                      state.createUserResult.id ?? "Id is Null")
                                  : state is Error
                                      ? Text(state.error)
                                      : Text("state is Empty")
                        ],
                      )
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
