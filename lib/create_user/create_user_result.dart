class CreateUserResult {
  bool? status;
  String? message;
  String? id;

  CreateUserResult({this.status, this.message, this.id});

  CreateUserResult.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    message = json['Message'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['Message'] = this.message;
    data['Id'] = this.id;
    return data;
  }
}
