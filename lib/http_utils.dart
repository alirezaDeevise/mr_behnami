import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';

class HttpUtils {
  Future<T> getData<T>(
      String url, T parseData(Map<String, dynamic> responseData)) async {
    Response response = await Dio().get(
      url,
    );
    Map<String, dynamic> decode = json.decode(response.data);

    return parseData(decode);
  }

  Future<T> postData<T>(
      {required String url,
      required T parseData(Map<String, dynamic> responseData),
      required Map<String, dynamic> body}) async {
    print(body);
    json.encode(body);
    Response response = await Dio().post(url,
        data: json.encode(body),
        options: Options(
            headers: {"Content-Type": "application/json", "accept": "*/*"}));
    Map<String, dynamic> decode = response.data;

    return parseData(decode);
  }

  Future<T> uploadFile<T>(
      {required String url,
      required File file,
      required T parseData(Map<String, dynamic> responseData)}) async {
    var formData = FormData.fromMap({
      'type': 1,
      'file': await MultipartFile.fromFile(file.path, filename: 'upload.png'),
    });
    Response response = await Dio().post(url, data: formData);
    Map<String, dynamic> decode = json.decode(response.data);

    return parseData(decode);
  }
}
