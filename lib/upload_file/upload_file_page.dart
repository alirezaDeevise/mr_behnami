import 'dart:io';

import 'package:behnami/http_utils.dart';
import 'package:behnami/upload_file/upload_file_result.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UploadFilePage extends StatefulWidget {
  const UploadFilePage({Key? key}) : super(key: key);

  @override
  _UploadFilePageState createState() => _UploadFilePageState();
}

class _UploadFilePageState extends State<UploadFilePage> {
  File? pickedImage;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Uplolad Image"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextButton(
                onPressed: () async {
                  final ImagePicker _picker = ImagePicker();
                  final XFile? photo =
                      await _picker.pickImage(source: ImageSource.gallery);
                  print(photo?.path ?? "null");
                  setState(() {
                    pickedImage = File(photo!.path);
                  });
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue)),
                child: Center(
                  child: Text(
                    "Pick Image",
                    style: TextStyle(color: Colors.white),
                  ),
                )),
            SizedBox(
              height: 16.0,
            ),
            pickedImage == null ? Icon(Icons.image) : Image.file(pickedImage!),
            SizedBox(
              height: 16.0,
            ),
            TextButton(
                onPressed: () async {
                  HttpUtils httpUtils = HttpUtils();
                  httpUtils
                      .uploadFile<UploadFileResult>(
                          url:
                              "http://fileapi.makanavenue.com/Document/SendDocument",
                          file: pickedImage!,
                          parseData: (responseData) =>
                              UploadFileResult.fromJson(responseData))
                      .then((value) => print(value.location));
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue)),
                child: Center(
                  child: Text(
                    "Upload image",
                    style: TextStyle(color: Colors.white),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
