class UploadFileResult {
  String? id;
  String? location;
  int? type;

  UploadFileResult({this.id, this.location, this.type});

  UploadFileResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    location = json['location'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['location'] = this.location;
    data['type'] = this.type;
    return data;
  }
}
