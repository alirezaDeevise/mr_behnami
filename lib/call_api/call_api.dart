import 'dart:convert';

import 'package:behnami/call_api/test_api_model.dart';
import 'package:behnami/call_api/test_dog_api_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class CallApi extends StatefulWidget {
  const CallApi({Key? key}) : super(key: key);

  @override
  _CallApiState createState() => _CallApiState();
}

class _CallApiState extends State<CallApi> {
  TestApi? testApi;
  DogApi? dogApi;
  bool isLoading = false;

  @override
  void initState() {
    getPageData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Call API"),
      ),
      body: isLoading ? callApiLoadingWidget() : callApiBodyWidget(),
    );
  }

  void getPageData() async {
    setState(() {
      isLoading = !isLoading;
    });
    TestApi result = await getData<TestApi>(
        "https://jsonplaceholder.typicode.com/todos/1", (decode) {
      return TestApi.fromJson(decode);
    });
    DogApi dogResult = await getData<DogApi>(
        "https://dog.ceo/api/breeds/image/random",
        (decode) => DogApi.fromJson(decode));

    setState(() {
      testApi = result;
      dogApi = dogResult;
      isLoading = !isLoading;
    });
  }

  Center callApiLoadingWidget() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Padding callApiBodyWidget() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "User id : ",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Text(
                testApi?.userId?.toString() ?? "",
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 22),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Id : ",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Text(
                testApi?.id?.toString() ?? "",
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 22),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Title : ",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Text(
                testApi?.title?.toString() ?? "",
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 22),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Completed : ",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Text(
                testApi?.completed?.toString() ?? "",
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 22),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          dogApi?.message == null
              ? Placeholder(
                  fallbackHeight: 100.0,
                  fallbackWidth: 100.0,
                )
              : Image.network(dogApi!.message!)
        ],
      ),
    );
  }

  Future<T> getData<T>(
      String url, T parseData(Map<String, dynamic> decode)) async {
    Response response = await http.get(
      Uri.parse(url),
    );
    Map<String, dynamic> decode = json.decode(response.body);

    return parseData(decode);
  }
}
